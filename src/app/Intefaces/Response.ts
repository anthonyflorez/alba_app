export interface ServerResponse {
    data:any;
    status:number;
    msg:string;
    jwt:string;
}