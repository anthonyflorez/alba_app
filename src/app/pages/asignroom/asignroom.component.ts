import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper/helper.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-asignroom',
  templateUrl: './asignroom.component.html',
  styleUrls: ['./asignroom.component.css']
})
export class AsignroomComponent implements OnInit {
  users: any;
  code:string;
  roomProperty: any;
  constructor(public helper:HelperService,public route:ActivatedRoute ,public loading:LoadingService, public alert:AlertService, public router:Router) { }

  ngOnInit(): void {
    if (typeof this.router.getCurrentNavigation().extras != 'undefined') {
      this.roomProperty = this.router.getCurrentNavigation().extras
      this.getUsers()
    }
  }

  getUsers(){
    this.loading.present()
    this.helper.get("private/property_staff_cleaning",this.roomProperty).then(response=>{
      this.users = response.data
      this.loading.dismiss()
    }).catch(error=>{
      this.loading.dismiss()
    })
  }

  addMember(user){
    let property =  JSON.parse(localStorage.getItem('property'));
    let user_property  = {id_user:user.id_user, id_property:property.id_property}
    this.loading.present()
    this.helper.post("private/property_staff",user_property).then(response=>{
      if (response.status==1) {
        this.router.navigate(['/managestaff']);
        this.alert.presentAlert("Aviso", response.msg)
      }
      this.loading.dismiss()
    }).catch(error=>{
      this.loading.dismiss()
      console.error(error);
      
    })
  }

  gotoAsign(user){
    let userRoom = {user, room:this.roomProperty }
    let navigationExtras: NavigationExtras = {
      state: {
        data: userRoom
      }
    };
    this.router.navigate(['confirmroom'],navigationExtras)
  }
}
