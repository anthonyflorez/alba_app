import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { FilterModule } from 'src/app/shared/filter/filter.module';
import { FormsModule } from '@angular/forms';
import { AsignroomComponent } from './asignroom.component';
import { AsignroomComponentRoutingModule } from './asignroom-routing.module';


@NgModule({
  declarations: [AsignroomComponent],
  imports: [
    CommonModule,
    AsignroomComponentRoutingModule,
    HeaderbackbuttonModule,
    SharedTranslateModule,
    HelperModule,
    FilterModule,
    FormsModule 
  ]
})
export class AsignRoomModule { }
