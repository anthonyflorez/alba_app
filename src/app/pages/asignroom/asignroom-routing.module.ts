import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsignroomComponent } from './asignroom.component';



const routes: Routes = [{ path: '', component: AsignroomComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsignroomComponentRoutingModule { }
