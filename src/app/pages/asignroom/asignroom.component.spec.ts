import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AsignroomComponent } from './asignroom.component';



describe('AsignroomComponent', () => {
  let component: AsignroomComponent;
  let fixture: ComponentFixture<AsignroomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignroomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
