import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Room } from 'src/app/models/Room';
import { Protocol } from 'src/app/models/Protocols';
import { HelperService } from 'src/app/services/helper/helper.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {
  @Output() dataSend : EventEmitter<any> = new EventEmitter();
  title: string;
  rooms:Room[] =[];
  protocols:Protocol[] =[];
  roomssection: boolean;
  protocolssection: boolean;
  type: number;


  constructor(public routea: ActivatedRoute, public loading:LoadingService, public helper:HelperService, public router:Router) { }

  ngOnInit(): void {
    this.routea.params.subscribe(data=>{
      let property = JSON.parse(localStorage.getItem('property'));
      if (data.type == 'rooms') {
        this.type = 1
        this.loading.present()
        this.helper.get('private/room_all',{id_property:property.id_property}).then(response=>{
          this.loading.dismiss()
          this.rooms =  response.data
        }).catch(error=>{
          console.error(error);
          this.loading.dismiss()
        })
        this.roomssection = true;
        this.title ='manage_rooms'
      }else if (data.type == 'protocols') {
        this.type = 2
        this.loading.present()
        this.helper.get('private/protocol_all',{id_property:property.id_property}).then(response=>{
          this.loading.dismiss()
          this.protocols =  response.data
        }).catch(error=>{
          console.error(error);
          this.loading.dismiss()
        })
        this.title ='manage_protocols'
        this.protocolssection = true;
      }
    })
  }

  gotoCreate(){
    switch (this.type) {
      case 1:
        this.router.navigate(['/createroom'])
        
        break;
      case 2:
        this.router.navigate(['/createprotocol'])

        break;
      
      default:
        break;
    }
  }

  getRoom(room:Room){
    let navigationExtras: NavigationExtras = {
      state: {
        data:room
      }
    };
    this.router.navigate(['/createroom'],navigationExtras);

  }

  getProtocol(protocol:Protocol){
    let navigationExtras: NavigationExtras = {
      state: {
        data:protocol
      }
    };
    this.router.navigate(['/createprotocol'],navigationExtras);
  }

}
