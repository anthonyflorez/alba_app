import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageRoutingModule } from './manage-routing.module';
import { ManageComponent } from './manage.component';
import { CardmanageModule } from 'src/app/components/cardmanage/cardmanage.module';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';


@NgModule({
  declarations: [ManageComponent],
  imports: [
    CommonModule,
    ManageRoutingModule,
    CardmanageModule,
    HeaderbackbuttonModule,
    HelperModule,
    SharedTranslateModule
  ]
})
export class ManageModule { }
