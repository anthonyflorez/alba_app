import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateprofileRoutingModule } from './createprofile-routing.module';
import { CreateprofileComponent } from './createprofile.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { FormuserModule } from 'src/app/components/formuser/formuser.module';


@NgModule({
  declarations: [CreateprofileComponent],
  imports: [
    CommonModule,
    CreateprofileRoutingModule,
    HeaderbackbuttonModule,
    FormuserModule
  ]
})
export class CreateprofileModule { }
