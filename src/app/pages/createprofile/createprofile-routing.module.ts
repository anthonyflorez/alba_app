import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateprofileComponent } from './createprofile.component';

const routes: Routes = [{ path: '', component: CreateprofileComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateprofileRoutingModule { }
