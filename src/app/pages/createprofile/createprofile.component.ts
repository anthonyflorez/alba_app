import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-createprofile',
  templateUrl: './createprofile.component.html',
  styleUrls: ['./createprofile.component.scss'],
})
export class CreateprofileComponent implements OnInit {
  constructor(public router:Router) { 
  }

  ngOnInit(): void {
  }

  getData(value){
    let navigationExtras: NavigationExtras = {
      state: {value:value, type:1}
    };
    this.router.navigate(['/choosejob'],navigationExtras);
  }
}
