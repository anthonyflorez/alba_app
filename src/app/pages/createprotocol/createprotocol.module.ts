import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateprotocolRoutingModule } from './createprotocol-routing.module';
import { CreateprotocolComponent } from './createprotocol.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { CardmanageModule } from 'src/app/components/cardmanage/cardmanage.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';


@NgModule({
  declarations: [CreateprotocolComponent],
  imports: [
    CommonModule,
    CreateprotocolRoutingModule,
    HeaderbackbuttonModule,
    CardmanageModule,
    HelperModule,
    FormsModule,
    ReactiveFormsModule,
    SharedTranslateModule
  ]
})
export class CreateprotocolModule { }
