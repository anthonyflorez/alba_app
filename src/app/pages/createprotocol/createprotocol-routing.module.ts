import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateprotocolComponent } from './createprotocol.component';

const routes: Routes = [{ path: '', component: CreateprotocolComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateprotocolRoutingModule { }
