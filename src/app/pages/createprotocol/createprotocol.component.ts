import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HelperService } from 'src/app/services/helper/helper.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { Protocol } from 'src/app/models/Protocols';

@Component({
  selector: 'app-createprotocol',
  templateUrl: './createprotocol.component.html',
  styleUrls: ['./createprotocol.component.css']
})
export class CreateprotocolComponent implements OnInit {
  formProtocol: FormGroup;
  protocol: any;
  isUpdate: boolean;
  constructor(public helper:HelperService, public route:ActivatedRoute, private formBuilder: FormBuilder, public alert:AlertService,public router:Router,public loading:LoadingService) { }

  ngOnInit(): void {
    if (typeof this.router.getCurrentNavigation().extras.state !='undefined') {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          let dataRoute=this.router.getCurrentNavigation().extras.state.data;
          this.isUpdate = true;
          this.protocol = dataRoute
          this.formProtocol =  this.formBuilder.group({
            name:[this.protocol.name, Validators.required]
          })
        }
      });
    }else{
      this.formProtocol =  this.formBuilder.group({
        name:['', Validators.required]
      })
    }
  }
  
  addProtocol(){
    this.loading.present()
    let property =  JSON.parse(localStorage.getItem('property'));
    this.helper.post('private/protocol',{id_property: property.id_property,name:this.formProtocol.value.name}).then(response=>{
      if(response.status==1){
        this.alert.presentAlert('Aviso','Se crea Correctamente el Protocolo')
        this.router.navigate(['/manage/protocols'])
      }
      this.loading.dismiss()
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss();
    })
  }

  updateProtocol(){
    this.loading.present()
    this.helper.put('private/protocol',{name:this.formProtocol.value.name, id_protocol: this.protocol.id_protocol}).then(response=>{
      if(response.status==1){
        this.alert.presentAlert('Aviso','Se actualiza Correctamente el Protocolo')
        this.router.navigate(['/manage/protocols'])
      }
      this.loading.dismiss()
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss();
    })
  }

  
  deteleProtocol(){
    let confirm  = this.alert.presentConfirm('Aviso','¿Desea eliminar el Protocolo?','No','Si');
    confirm.then(data=>{
      if (data) {
        this.loading.present()
        this.helper.delete('private/protocol',this.protocol).then(response=>{
          if(response.status==1){
            this.alert.presentAlert('Aviso','Se ha Eliminado Correctamente el Protocolo.')
            this.router.navigate(['/manage/protocols'])
          }
          this.loading.dismiss();
        }).catch(error=>{
          console.error(error);
          this.loading.dismiss();
        })
      }
    })
  }
}
