import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PropertiesRoutingModule } from './properties-routing.module';
import { PropertiesComponent } from './properties.component';
import { HeaderwelcomeModule } from 'src/app/components/headerwelcome/headerwelcome.module';
import { CardchooseModule } from 'src/app/components/cardchoose/cardchoose.module';
import { CardnamehotelModule } from 'src/app/components/cardnamehotel/cardnamehotel.module';
import { HelperModule } from 'src/app/services/helper/helper.module';


@NgModule({
  declarations: [PropertiesComponent],
  imports: [
    CommonModule,
    PropertiesRoutingModule,
    HeaderwelcomeModule,
    CardchooseModule,
    CardnamehotelModule,
    HelperModule
  ]
})
export class PropertiesModule { }
