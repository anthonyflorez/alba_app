import { Component, OnInit } from '@angular/core';
import { Choose } from 'src/app/models/Choose';
import { Profile } from 'src/app/models/Profile';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { HelperService } from 'src/app/services/helper/helper.service';
import { toTypeScript } from '@angular/compiler';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.css']
})
export class PropertiesComponent implements OnInit {
  chooses:Choose[] =[
    { id_rol: "1",title:"createproperty",img: "assets/img/property.svg", selected:false},
    { id_rol: "2",title:"manageproperty",img: "assets/img/manageproperty.png", selected:false},
    { id_rol: "3",title:"managestaff",img: "assets/img/managestaff.png", selected:false},
    { id_rol: "4",title:"staffreports",img: "assets/img/staffreports.png", selected:false},
  ]
  card1:any ={name:'Celestino', img:'assets/img/celestino.png'}
  card2:any ={name:'Celestino', img:'assets/img/calendar.png'}
  profile:Profile = new Profile ;
  properties: any[] =[];
  propertySelect: any ={name:'Celestino', img:'assets/img/celestino.png'};
  openproperties: boolean;
  is_more_one: boolean;

  constructor(public router:Router,public actionSheetController: ActionSheetController, public helper:HelperService) { }

  ngOnInit(): void {
    // let property = JSON.parse(localStorage.getItem('property'))
    this.getProperties()
  }
  
  getProperties(){
    this.helper.get('private/property_all').then(response=>{
      this.properties = response.data
      this.is_more_one = (response.data.length == 1) ? false: true
      if(response.data.length > 0) {
        localStorage.setItem('property', JSON.stringify(response.data[0]));
      }
      let property = localStorage.getItem('property');
      if (this.properties.length > 0) {
        this.propertySelect = response.data[0]
        this.properties = this.properties.map((e,i)=>{
          e.img =  'assets/img/celestino.png'
          return e
        })
      }
      this.propertySelect.img  = 'assets/img/celestino.png'
    }).catch(error=>{
      console.error(error);
    })
  }


  /**
   * 
   * @param choose 
   * @param i 
   */
  Selected(choose:Choose, i){
    this.chooses.forEach(e=>{
      e.selected = false
    })
    this.chooses[i].selected = true
    this.profile.id_rol = Number(choose.id_rol)

    switch (choose.id_rol) {
      case "1":
        this.router.navigate(['/createproperty'])
        break;
      case "2":
        this.presentActionSheet();
        break;
      case "3":
        this.router.navigate(['/managestaff'])
        break;
      case "4":
        this.router.navigate(['/reportstaff'])
        break;
    
      default:
        break;
    }

    
   
    
  }
  /**
   * 
   */
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'action_sheet_custom',
      buttons: [
        {
        text: 'Gestionar Habitación',
        cssClass:'btn_blue_white_action_sheet',
        handler: () => {
          this.router.navigate(['/manage/rooms'])
        }},
        {
        text: 'Gestionar Protocolos',
        cssClass:'btn_blue_white_action_sheet',
        handler: () => {
          this.router.navigate(['/manage/protocols'])

        }}
    ]
    });
    await actionSheet.present();
  }
  
  gotoAdmin(){
    this.router.navigate(['/admin'])
  }


  openListProperties(){
    if (this.is_more_one) {
      this.openproperties = !this.openproperties;
    }
  }

  SelectProperty(p){
    this.properties.forEach((element,i)=>{
      if (element.id_property== p.id_property) {
        this.properties[i] = this.propertySelect
      }
      return element
    })
    this.openproperties = false;
    this.propertySelect = p
    localStorage.setItem('property',JSON.stringify(this.propertySelect))

  }

}
