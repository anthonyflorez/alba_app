import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportstaffComponent } from './reportstaff.component';

const routes: Routes = [{ path: '', component: ReportstaffComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportstaffRoutingModule { }
