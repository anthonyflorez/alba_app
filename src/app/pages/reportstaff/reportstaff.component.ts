import { Component, OnInit } from '@angular/core';
import { Choose } from 'src/app/models/Choose';

@Component({
  selector: 'app-reportstaff',
  templateUrl: './reportstaff.component.html',
  styleUrls: ['./reportstaff.component.css']
})
export class ReportstaffComponent implements OnInit {
  chooses:Choose[] =[
    { id_rol: "1",title:"Hours",img: "", selected:false},
    { id_rol: "2",title:"Performance",img: "", selected:false},
    { id_rol: "3",title:"Maintance",img: "", selected:false},
    { id_rol: "4",title:"Future",img: "", selected:false},
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
