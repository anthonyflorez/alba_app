import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportstaffRoutingModule } from './reportstaff-routing.module';
import { ReportstaffComponent } from './reportstaff.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { CardchooseModule } from 'src/app/components/cardchoose/cardchoose.module';


@NgModule({
  declarations: [ReportstaffComponent],
  imports: [
    CommonModule,
    ReportstaffRoutingModule,
    HeaderbackbuttonModule,
    CardchooseModule
  ]
})
export class ReportstaffModule { }
