import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportstaffComponent } from './reportstaff.component';

describe('ReportstaffComponent', () => {
  let component: ReportstaffComponent;
  let fixture: ComponentFixture<ReportstaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportstaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportstaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
