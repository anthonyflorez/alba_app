import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChoosejobComponent } from './choosejob.component';

const routes: Routes = [{ path: '', component: ChoosejobComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChoosejobRoutingModule { }
