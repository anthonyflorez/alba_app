import { Component, OnInit } from '@angular/core';
import { Choose } from 'src/app/models/Choose';
import { ActivatedRoute, Router } from '@angular/router';
import { Profile } from 'src/app/models/Profile';
import { HelperService } from 'src/app/services/helper/helper.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ImageService } from 'src/app/services/image/image.service';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-choosejob',
  templateUrl: './choosejob.component.html',
  styleUrls: ['./choosejob.component.css']
})
export class ChoosejobComponent implements OnInit {
  chooses:Choose[] =[
    { id_rol: "1",title:"administrator",img: "assets/img/administrator.png", selected:false},
    { id_rol: "2",title:"reception",img: "assets/img/reception.png", selected:false},
    { id_rol: "3",title:"cleaning",img: "assets/img/cleaning.png", selected:false},
    { id_rol: "4",title:"maintance",img: "assets/img/maintance.png", selected:false},
  ]
  profile:Profile = new Profile ;
  type: any;
  ImageObj: any;
  constructor(public route: ActivatedRoute,public alert:AlertService,public datas:DataService, public imageservice:ImageService, public helper:HelperService, public loading:LoadingService , public router:Router) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.profile = this.router.getCurrentNavigation().extras.state.value.form;
        this.ImageObj = this.router.getCurrentNavigation().extras.state.value.image;
        this.type = this.router.getCurrentNavigation().extras.state.type;
      }
    })

  }

  Selected(choose:Choose, i){
    this.chooses.forEach(e=>{
      e.selected = false
    })
    this.chooses[i].selected = true
    this.profile.id_rol = Number(choose.id_rol) 
  }
  
  back() {
    this.router.navigate(['updateprofile']);
  }

  createProfile(){
    if (this.validateSelect()) {
      delete this.profile.rpassword;
      console.log(this.profile.image);
      this.loading.present()
      this.helper.post("public/register",this.profile).then(response=>{
        localStorage.setItem('token', response.jwt)
        if (typeof this.ImageObj !='undefined') {
          if (this.ImageObj.type == 1) {
            this.helper.uploadImage('private/prueba',this.ImageObj.blobData, this.ImageObj.imageName, this.ImageObj.format).then(response=>{
            })
          }else if(this.ImageObj.type == 2){
            this.helper.uploadImageFile('private/prueba',this.ImageObj.file).then(response=>{
            })
          }
        }
        this.router.navigate(['/properties'])
        this.loading.dismiss()
      }).catch(error=>{
        console.error(error)
        this.loading.dismiss()
      })
      
    }else{
      console.log("debes seleccionar ");
      
    }
   
  }

  updateProfile(){
    console.log(this.validateSelect());
    
    if (this.validateSelect()) {
      delete this.profile.rpassword;
      this.loading.present()
      this.helper.put("private/user",this.profile).then(response=>{
        if (response.status == 1) {
          this.router.navigate(['/properties'])
        }
        this.loading.dismiss()
      }).catch(error=>{
        console.error(error)
        this.loading.dismiss()
      })
      
    }else{
      this.alert.presentAlert('Aviso','Debe seleccionar un item')
    }
    
  }

  validateSelect() : any {
    return this.chooses.filter(e=>{
      return (e.selected == true)   
    })
  }

}
