import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChoosejobRoutingModule } from './choosejob-routing.module';
import { ChoosejobComponent } from './choosejob.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { CardchooseModule } from 'src/app/components/cardchoose/cardchoose.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';
import { ImageModule } from 'src/app/services/image/image.module';
import { DataModule } from 'src/app/services/data/data.module';
import { DataService } from 'src/app/services/data/data.service';

@NgModule({
  declarations: [ChoosejobComponent],
  imports: [
    CommonModule,
    ChoosejobRoutingModule,
    HeaderbackbuttonModule,
    CardchooseModule,
    ReactiveFormsModule,
    FormsModule,
    HelperModule,
    SharedTranslateModule,
    ImageModule,
  ],
  providers:[DataService]
})
export class ChoosejobModule { }
