import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosejobComponent } from './choosejob.component';

describe('ChoosejobComponent', () => {
  let component: ChoosejobComponent;
  let fixture: ComponentFixture<ChoosejobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosejobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosejobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
