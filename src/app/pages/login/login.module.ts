import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { LoadingModule } from 'src/app/services/loading/loading.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HelperModule,
    LoadingModule,
    SharedTranslateModule
  ],
})
export class LoginModule { }
