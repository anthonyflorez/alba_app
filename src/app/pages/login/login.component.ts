import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperService } from 'src/app/services/helper/helper.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  coordinates: any;
  constructor(public router:Router,public formBuilder: FormBuilder, public helper:HelperService, public loading: LoadingService, public alertController: AlertController) { }

  ngOnInit(): void {
    this.initFormGroup() 
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

  initFormGroup(){
    this.loginForm = this.formBuilder.group({
      code: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  

  createProfile(){
    this.router.navigate(['/createprofile'])
  }

  async login(){

    this.loading.present();
    if (this.loginForm.valid) {
      this.helper.post("public/login", this.loginForm.value).then(async response=>{
        if (response.status == 1) {
          if(typeof response.data.img != 'undefined'){
            localStorage.setItem('user',JSON.stringify(response.data))
          } 
          localStorage.setItem('token', response.jwt)
          switch (response.data.id_rol) {
            case 1:
            this.router.navigate(['/properties']);
              break;
            case 2:
            this.router.navigate(['/admin']);
              break;
            case 3:
            this.router.navigate(['/home']);
              break;
            case 4:
            this.router.navigate(['/home']);
              break;
            default:
              break;
          }
        }
        if(response.status == 40) {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Error',
            message: response.msg,
            buttons: ['OK']
          });
          alert.present();
        }
        this.loading.dismiss();
      }).catch(error=>{
        this.loading.dismiss()
        this.initFormGroup() 
      })
    }
    
  }

}
