import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagestaffRoutingModule } from './managestaff-routing.module';
import { ManagestaffComponent } from './managestaff.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';


@NgModule({
  declarations: [ManagestaffComponent],
  imports: [
    CommonModule,
    ManagestaffRoutingModule,
    HeaderbackbuttonModule,
    HelperModule,
    SharedTranslateModule
    
  ]
})
export class ManagestaffModule { }
