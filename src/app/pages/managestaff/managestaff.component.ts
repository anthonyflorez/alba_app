import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper/helper.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-managestaff',
  templateUrl: './managestaff.component.html',
  styleUrls: ['./managestaff.component.css']
})
export class ManagestaffComponent implements OnInit {
  properties: any;

  constructor(public helper:HelperService, public router:Router) {}

  ngOnInit(): void {
    console.log('entró');
    this.getStaff();
  }

  getStaff(){
    let property  = JSON.parse(localStorage.getItem("property"));    
    this.helper.get('private/property_staff',property).then(response=>{
      this.properties =response.data
      
    })
  }

  addStaffMember(){
    this.router.navigate(['addstaffmember'])
  }

}
