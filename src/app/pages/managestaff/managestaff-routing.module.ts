import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagestaffComponent } from './managestaff.component';

const routes: Routes = [{ path: '', component: ManagestaffComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagestaffRoutingModule { }
