import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';
import { ConfirmroomComponent } from './confirmroom.component';
import { ConfirmroomRoutingModule } from './confirmroom-routing.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ConfirmroomComponent],
  imports: [
    CommonModule,
    ConfirmroomRoutingModule, 
    HeaderbackbuttonModule,
    SharedTranslateModule,
    FormsModule,
    ReactiveFormsModule,
    HelperModule
  ]
})
export class ConfirmroomModule { }
