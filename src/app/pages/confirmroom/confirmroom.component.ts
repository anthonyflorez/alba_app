import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper/helper.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert/alert.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-confirmroom',
  templateUrl: './confirmroom.component.html',
  styleUrls: ['./confirmroom.component.css']
})
export class ConfirmroomComponent implements OnInit {
  typeCleaning: any;
  formConfirmRoom: FormGroup;
  dataRoute: any;

  constructor(public helper:HelperService,  private formBuilder: FormBuilder,public alert:AlertService, public loading:LoadingService, public router:Router) { 
    if (typeof this.router.getCurrentNavigation().extras.state !='undefined') {
      this.dataRoute = this.router.getCurrentNavigation().extras.state.data
    }
  }


  ngOnInit(): void {
    this.formConfirmRoom = this.formBuilder.group({
      id_cleaning_type:['', Validators.required],
      admin_comment:['']
    });
    this.getTypeCleaning()
  }

  getTypeCleaning(){
    this.helper.get("private/cleaning_type").then(response=>{
      this.typeCleaning = response.data
    })
  }

  changeSelect(option){
    this.formConfirmRoom.controls.id_cleaning_type.setValue(option.detail.value)
  }

  asignRoom(){ 
    this.formConfirmRoom.value.id_property_room = this.dataRoute.room.id_property_room
    this.formConfirmRoom.value.id_user = this.dataRoute.user.id_user
    console.log(this.formConfirmRoom);
    
    if (this.formConfirmRoom.valid) {
      this.loading.present()
      this.helper.post("private/property_schedule",this.formConfirmRoom.value).then(response=>{
        if (response.status==1) {
          this.router.navigate(['admin']);
        }
        this.alert.presentAlert("Aviso",response.msg)
        this.loading.dismiss()
      })
    }
  }
}
