import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfirmroomComponent } from './confirmroom.component';


const routes: Routes = [{ path: '', component: ConfirmroomComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfirmroomRoutingModule { }
