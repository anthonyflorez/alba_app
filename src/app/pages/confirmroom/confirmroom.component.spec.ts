import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfirmroomComponent } from './confirmroom.component';



describe('ConfirmroomComponent', () => {
  let component: ConfirmroomComponent;
  let fixture: ComponentFixture<ConfirmroomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmroomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
