import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChooselocationRoutingModule } from './chooselocation-routing.module';
import { ChooselocationComponent } from './chooselocation.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from "@agm/core";
import { HttpClientModule } from '@angular/common/http';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';

@NgModule({
  declarations: [ChooselocationComponent],
  imports: [
    CommonModule,
    ChooselocationRoutingModule,
    GooglePlaceModule,
    HttpClientModule,
    HeaderbackbuttonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyASNYO_5y8YPTqwAeJESJKELt40CVTTMBc',
      libraries: ['places']
    }),
    SharedTranslateModule
  ]
})
export class ChooselocationModule { }
