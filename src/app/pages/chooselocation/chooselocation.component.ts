import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ToastController } from "@ionic/angular";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { google } from "google-maps";
import { Router, NavigationExtras } from '@angular/router';
import { Plugins } from '@capacitor/core';

const { Geolocation } = Plugins;
declare var google : google;

@Component({
  selector: 'app-chooselocation',
  templateUrl: './chooselocation.component.html',
  styleUrls: ['./chooselocation.component.css']
})
export class ChooselocationComponent implements OnInit {
  @ViewChild('gmap') gmapElement: any;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  lat: number;
  lng: number;
  address: any;
  islocationselect: boolean;
  coordinates: any;

  constructor(
    private http: HttpClient,
    public toastController: ToastController,
    public router:Router
  ) {}

  ngOnInit() {

  }

  async getCurrentPosition() {
    this.coordinates = await Geolocation.getCurrentPosition();
     this.lat = this.coordinates.coords.latitude;
     this.lng = this.coordinates.coords.longitude;
     this.initMap();
   }

  handleAddressChange(place){
    this.address = place.formatted_address
    this.lat = place.geometry.location.lat();
    this.lng = place.geometry.location.lng();
    this.initMap();
  }

  initMap() {
    var uluru = {lat:this.lat, lng: this.lng};
    var map = new google.maps.Map(
    document.getElementById('map'), {zoom: 18, center: uluru});
    var marker = new google.maps.Marker({position: uluru, map: map});
    this.islocationselect =true
  }

  gotoCreate(){
    let navigationExtras: NavigationExtras = {
      state: {
        data: {lat:this.lat, lng: this.lng,address:this.address}
      }
    };
    this.router.navigate(['/createproperty'],navigationExtras);
  }
}
