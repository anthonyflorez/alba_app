import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooselocationComponent } from './chooselocation.component';

const routes: Routes = [{ path: '', component: ChooselocationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChooselocationRoutingModule { }
