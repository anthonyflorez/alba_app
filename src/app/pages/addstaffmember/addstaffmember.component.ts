import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper/helper.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-addstaffmember',
  templateUrl: './addstaffmember.component.html',
  styleUrls: ['./addstaffmember.component.css']
})
export class AddstaffmemberComponent implements OnInit {
  users: any;
  code:string;
  constructor(public helper:HelperService, public loading:LoadingService, public alert:AlertService, public router:Router) { }

  ngOnInit(): void {
    this.getUsers()
  }

  getUsers(){
    this.helper.get("private/available_users").then(response=>{
      this.users = response.data
    })
  }

  addMember(user){
    let property =  JSON.parse(localStorage.getItem('property'));
    let user_property  = {id_user:user.id_user, id_property:property.id_property}
    this.loading.present()
    this.helper.post("private/property_staff",user_property).then(response=>{
      if (response.status==1) {
        this.router.navigate(['/managestaff']);
      }
      this.loading.dismiss()
    }).catch(error=>{
      this.loading.dismiss()
    })
  }
}
