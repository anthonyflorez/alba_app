import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddstaffmemberRoutingModule } from './addstaffmember-routing.module';
import { AddstaffmemberComponent } from './addstaffmember.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { FilterModule } from 'src/app/shared/filter/filter.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [AddstaffmemberComponent],
  imports: [
    CommonModule,
    AddstaffmemberRoutingModule,
    HeaderbackbuttonModule,
    SharedTranslateModule,
    HelperModule,
    FilterModule,
    FormsModule 
  ]
})
export class AddstaffmemberModule { }
