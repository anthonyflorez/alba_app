import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddstaffmemberComponent } from './addstaffmember.component';

describe('AddstaffmemberComponent', () => {
  let component: AddstaffmemberComponent;
  let fixture: ComponentFixture<AddstaffmemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddstaffmemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddstaffmemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
