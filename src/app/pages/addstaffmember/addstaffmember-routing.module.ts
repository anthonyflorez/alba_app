import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddstaffmemberComponent } from './addstaffmember.component';

const routes: Routes = [{ path: '', component: AddstaffmemberComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddstaffmemberRoutingModule { }
