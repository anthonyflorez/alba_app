import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HeaderwelcomeModule } from 'src/app/components/headerwelcome/headerwelcome.module';
import { CardnamehotelModule } from 'src/app/components/cardnamehotel/cardnamehotel.module';
import { TabsModule } from 'src/app/components/tabs/tabs.module';
import { CarditemModule } from 'src/app/components/carditem/carditem.module';
import { CarditemtodoModule } from 'src/app/components/carditemtodo/carditemtodo.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';
import { CardcompleteModule } from 'src/app/components/cardcomplete/cardcomplete.module';
import { CardhoursModule } from 'src/app/components/cardhours/cardhours.module';
import { HelperModule } from 'src/app/services/helper/helper.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HeaderwelcomeModule,
    CardnamehotelModule,
    TabsModule,
    CarditemModule, 
    CarditemtodoModule,
    SharedTranslateModule,
    CardcompleteModule,
    CardhoursModule,
    HelperModule
  ]
})
export class HomeModule { }
