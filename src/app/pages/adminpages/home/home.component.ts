import { Component, OnInit, EventEmitter } from '@angular/core';
import { Tab } from 'src/app/models/Tabs';
import { Item } from 'src/app/models/Item';
import { ActionSheetController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { HelperService } from 'src/app/services/helper/helper.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  cardName:any ={name:'Celestino Hotel', img:'assets/img/hotelimg.png'}
  tabs:Tab[] = [
    { id:1,name:'to_do',selected:false },
    { id:2,name:'completed',selected:false },
    { id:3,name:'inventory',selected:false },
  ]
  isTodo: boolean;
  isCompleted: boolean;
  isInventory: boolean;
  items1:any[] = [
    {id:1,img:'assets/img/img_profile.png',number:201,name:'Natalia',sendItem:new EventEmitter(), accept:true, color:'blue'},
    {id:2,img:'assets/img/img_profile.png',number:202,name:'Laura',sendItem:new EventEmitter(), accept:false,  color:'blue'},
    {id:3,img:'assets/img/img_profile.png',number:203,name:'Maria', sendItem:new EventEmitter(), accept:false,  color:'red'},
    {id:4,img:'assets/img/img_profile.png',number:204,name:'Pedro',sendItem:new EventEmitter(), accept:true, color:'blue'},
  ];
  items2:Item[] = [
    {id:1,img:'assets/img/img_profile.png',number:301,name:'Jose',sendItem:new EventEmitter()},
    {id:2,img:'assets/img/img_profile.png',number:302,name:'Antonio',sendItem:new EventEmitter()},
    {id:3,img:'assets/img/img_profile.png',number:303,name:'Alejandro',sendItem:new EventEmitter()},
    {id:4,img:'assets/img/img_profile.png',number:304,name:'Juan',sendItem:new EventEmitter()},
  ];
  items3:Item[] = [
    {id:1,img:'assets/img/img_profile.png',number:401,name:'Natalia',sendItem:new EventEmitter()},
    {id:2,img:'assets/img/img_profile.png',number:402,name:'Laura',sendItem:new EventEmitter()},
    {id:3,img:'assets/img/img_profile.png',number:403,name:'Maria',sendItem:new EventEmitter()},
    {id:4,img:'assets/img/img_profile.png',number:404,name:'Pedro',sendItem:new EventEmitter()},
  ];
  isStart:boolean
  scheduleStatus: any;
  constructor(public actionSheetController: ActionSheetController,public loading:LoadingService ,public router:Router,public helper: HelperService) { }

  ngOnInit(): void {
    this.loading.present()
    this.helper.get("private/user_schedule").then(response=>{
      this.scheduleStatus =  response.data
      this.loading.dismiss()
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss()
    })

    this.getTodoData()
  }



  getTodoData(){
    this.loading.present()
    let user =  JSON.parse(localStorage.getItem("user"));
    this.helper.get("private/property_schedule_user",user).then(response=>{
      if(typeof response.data != 'undefined'){
        this.items1 = response.data 
      }
      this.loading.dismiss()
    }).catch(error=>{
      this.loading.dismiss()
    })
  }

  selectItem(tab: Tab){
    switch (tab.id) {
      case 1:
        this.setOption(true, false, false)
        break;
    
      case 2:
        this.setOption(false, true, false)
        break;
    
      case 3:
        this.setOption(false, false, true)
        break;
    
      default:
        break;
    }
  }

  setOption(isTodo, isCompleted,isInventory){
    this.isTodo = isTodo
    this.isCompleted = isCompleted
    this.isInventory = isInventory
  }

  changeStatus(status){
    this.isStart = !status;
  }

  getDataTodoItem(value){
    console.log(value);
    this.presentActionSheet(value);
    
  }
  /**
   * 
   */
  async presentActionSheet(value) {
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'action_sheet_custom_accept',
      header: 'Aceptar ' + value.number + '?',
      buttons: [
        {
        text: 'No',
        icon: 'close-outline',
        cssClass:'btn_red_red',
        handler: () => {
          // this.router.navigate(['/manage/rooms'])
        }},
        {
        text: 'Si',
        icon:'checkmark-outline',
        cssClass:'btn_green_green',
        handler: () => {
          let navigationExtras: NavigationExtras = value;
          this.router.navigate(['/protocolbyroom'],navigationExtras)

        }}
    ]
    });
    await actionSheet.present();
  }

}
