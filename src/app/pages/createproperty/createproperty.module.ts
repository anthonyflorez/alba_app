import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreatepropertyRoutingModule } from './createproperty-routing.module';
import { CreatepropertyComponent } from './createproperty.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { DataModule } from 'src/app/services/data/data.module';
import { AlertModule } from 'src/app/services/alert/alert.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';


@NgModule({
  declarations: [CreatepropertyComponent],
  imports: [
    CommonModule,
    CreatepropertyRoutingModule,
    HeaderbackbuttonModule,
    FormsModule,
    ReactiveFormsModule,
    HelperModule,
    DataModule,
    AlertModule,
    SharedTranslateModule
  ]
})
export class CreatepropertyModule { }
