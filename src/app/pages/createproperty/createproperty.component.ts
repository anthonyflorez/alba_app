import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { google } from '@google/maps';
import { Router, ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/services/helper/helper.service';
import { DataService } from 'src/app/services/data/data.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AlertService } from 'src/app/services/alert/alert.service';

@Component({
  selector: 'app-createproperty',
  templateUrl: './createproperty.component.html',
  styleUrls: ['./createproperty.component.css']
})
export class CreatepropertyComponent implements OnInit {
  createpropertyform: FormGroup;
  GoogleAutocomplete: google.maps.places.AutocompleteService;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any
  constructor(public formBuilder: FormBuilder, public router: Router, public alert:AlertService, public loading: LoadingService,public datas:DataService, public route:ActivatedRoute, public helper:HelperService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        let dataRoute=this.router.getCurrentNavigation().extras.state.data;
        let dataPersis = this.datas.getData();
        this.createpropertyform = this.formBuilder.group({
          name:[dataPersis.name,Validators.required],
          id_property_type:[dataPersis.id_property_type, Validators.required],
          address:[dataRoute.address,Validators.required],
          latitude:[dataRoute.lat,Validators.required],
          longitude:[dataRoute.lng,Validators.required],
        });
        
        
      }
    });
   }

  ngOnInit(): void {
    this.createpropertyform = this.formBuilder.group({
      name:['',Validators.required],
      id_property_type:['', Validators.required],
      address:['Agregar Ubicación',Validators.required],
      latitude:['',Validators.required],
      longitude:['',Validators.required],
    });
  }
  chooseLocation(){
    this.datas.setData(this.createpropertyform.value)
    this.router.navigate(['/chooselocation'])
    // this.createpropertyform.controls['address'].setValue("Cualquier direccion");
    // this.createpropertyform.controls['latitude'].setValue(6.252801387407175);
    // this.createpropertyform.controls['longitude'].setValue(-75.558694144022);
  }
  
  changeSelect(select){
    this.createpropertyform.controls['id_property_type'].setValue(select.detail.value);
  }

  createProperty(){
    this.loading.present();
    this.helper.post('private/property', this.createpropertyform.value).then(response=>{
      if (response.status == 1) {
        this.alert.presentAlert("Aviso","La Propiedad se ha agregado Exitosamente")
        this.router.navigate(['/properties'])
      }
      this.loading.dismiss();
    }).catch(error=>{
      this.loading.dismiss();
      console.error(error);
    })
  }
}
