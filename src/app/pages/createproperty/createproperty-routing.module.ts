import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatepropertyComponent } from './createproperty.component';

const routes: Routes = [{ path: '', component: CreatepropertyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreatepropertyRoutingModule { }
