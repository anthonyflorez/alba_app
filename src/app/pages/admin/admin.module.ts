import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HeaderwelcomeModule } from 'src/app/components/headerwelcome/headerwelcome.module';
import { CardnamehotelModule } from 'src/app/components/cardnamehotel/cardnamehotel.module';
import { TabsModule } from 'src/app/components/tabs/tabs.module';
import { CarditemModule } from 'src/app/components/carditem/carditem.module';
import { CarditemtodoModule } from 'src/app/components/carditemtodo/carditemtodo.module';
import { CardcompleteModule } from 'src/app/components/cardcomplete/cardcomplete.module';
import { CardinventoryModule } from 'src/app/components/cardinventory/cardinventory.module';
import { HelperModule } from 'src/app/services/helper/helper.module';


@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    HeaderwelcomeModule,
    CardnamehotelModule,
    TabsModule,
    CardnamehotelModule,
    CarditemModule, 
    CarditemtodoModule,
    CardcompleteModule,
    CardinventoryModule,
    HelperModule
  ]
})
export class AdminModule { }
