import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Choose } from 'src/app/models/Choose';
import { Tab } from 'src/app/models/Tabs';
import { Item } from 'src/app/models/Item';
import { HelperService } from 'src/app/services/helper/helper.service';
import { Router } from '@angular/router';
import { ValueTransformer } from '@angular/compiler/src/util';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AlertService } from 'src/app/services/alert/alert.service';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  cardName:any ={name:'Celestino Hotel', img:'assets/img/hotelimg.png'}
  choose:Choose = { id_rol: "1",title:"Administrator",img: "assets/img/administrator.png", selected:false}
  tabs:Tab[] = [
    { id:1,name:'to_do',selected:false },
    { id:2,name:'completed',selected:false },
    { id:3,name:'inventory',selected:false },
  ]
  isTodo: boolean;
  isCompleted: boolean;
  isInventory: boolean;
  items1:any[] = [
    {id:1,img:'assets/img/img_profile.png',number:201,name:'Natalia',sendItem:new EventEmitter(), accept:true, color:'blue'},
    {id:2,img:'assets/img/img_profile.png',number:202,name:'Laura',sendItem:new EventEmitter(), accept:false,  color:'blue'},
    {id:3,img:'assets/img/img_profile.png',number:203,name:'Maria', sendItem:new EventEmitter(), accept:false,  color:'red'},
    {id:4,img:'assets/img/img_profile.png',number:204,name:'Pedro',sendItem:new EventEmitter(), accept:true, color:'blue'},
  ];
  items2:Item[] = [
    {id:1,img:'assets/img/img_profile.png',number:301,name:'Jose',sendItem:new EventEmitter()},
    {id:2,img:'assets/img/img_profile.png',number:302,name:'Antonio',sendItem:new EventEmitter()},
    {id:3,img:'assets/img/img_profile.png',number:303,name:'Alejandro',sendItem:new EventEmitter()},
    {id:4,img:'assets/img/img_profile.png',number:304,name:'Juan',sendItem:new EventEmitter()},
  ];
  items3:Item[] = [
    {id:1,img:'assets/img/img_profile.png',number:401,name:'Natalia',sendItem:new EventEmitter()},
    {id:2,img:'assets/img/img_profile.png',number:402,name:'Laura',sendItem:new EventEmitter()},
    {id:3,img:'assets/img/img_profile.png',number:403,name:'Maria',sendItem:new EventEmitter()},
    {id:4,img:'assets/img/img_profile.png',number:404,name:'Pedro',sendItem:new EventEmitter()},
  ];
  is_more_one: boolean;
  properties: any[] =[];
  propertySelect: any ={name:'Celestino', img:'assets/img/celestino.png'};
  openproperties: boolean;
  constructor(public helper:HelperService, public  router: Router, public loading:LoadingService, public alert:AlertService) { } 

  ngOnInit(): void {
    this.setOption(true, false, false)
    this.getProperties()
    this. getTodoData()
    this.getRoombyProperty(JSON.parse(localStorage.getItem('property')))
  }

  getProperties(){
    this.loading.present()
    this.helper.get("private/property_all").then(response=>{
      this.properties = response.data
      this.is_more_one = (response.data.length == 1) ? false: true
      let property =  localStorage.getItem('property');
      if (typeof property !='undefined') {
        this.propertySelect = JSON.parse(localStorage.getItem('property'))
      }else{
        if (this.properties.length > 0) {
          this.propertySelect =  response.data[0]
          this.properties = this.properties.map((e,i)=>{
            e.img =  'assets/img/celestino.png'
            return e
          })
        }
      }
      this.propertySelect.img  = 'assets/img/celestino.png'
      this.loading.dismiss()
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss()
    })
  }

  getTodoData(){
    this.loading.present()
    let property =  JSON.parse(localStorage.getItem("property"));
    this.helper.get("private/property_schedule_property",property).then(response=>{
      if(typeof response.data != 'undefined'){
        this.items1 = response.data 
      }
      this.loading.dismiss()
    }).catch(error=>{
      this.loading.dismiss()
    })
  }

  getRoombyProperty(property){
    this.loading.present();
    this.helper.get('private/room_all',property).then(response=>{
      this.items3 = response.data
      this.loading.dismiss()
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss()
    })
  }
  

  selectItem(tab: Tab){
    switch (tab.id) {
      case 1:
        this.setOption(true, false, false)
        break;
    
      case 2:
        this.setOption(false, true, false)
        break;
    
      case 3:
        this.setOption(false, false, true)
        break;
    
      default:
        break;
    }
  }

  setOption(isTodo, isCompleted,isInventory){
    this.isTodo = isTodo
    this.isCompleted = isCompleted
    this.isInventory = isInventory
  }

  SelectProperty(p){
    this.properties.forEach((element,i)=>{
      if (element.id_property== p.id_property) {
        this.properties[i] = this.propertySelect
      }
      return element
    })
    this.openproperties = false;
    this.propertySelect = p
    localStorage.setItem('property',JSON.stringify(this.propertySelect))

  }

  openListProperties(){
    if (this.is_more_one) {
      this.openproperties = !this.openproperties;
    }
  }

  getRoomInventory(value){
    this.router.navigate(['/asignroom'], value)
  }
}
