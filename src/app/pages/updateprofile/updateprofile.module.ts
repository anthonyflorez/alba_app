import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpdateprofileRoutingModule } from './updateprofile-routing.module';
import { UpdateprofileComponent } from './updateprofile.component';
import { FormuserModule } from 'src/app/components/formuser/formuser.module';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { HelperModule } from 'src/app/services/helper/helper.module';


@NgModule({
  declarations: [UpdateprofileComponent],
  imports: [
    CommonModule,
    UpdateprofileRoutingModule,
    HeaderbackbuttonModule,
    FormuserModule,
    HelperModule
  ]
})
export class UpdateprofileModule { }
