import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HelperService } from 'src/app/services/helper/helper.service';
import { Profile } from 'src/app/models/Profile';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-updateprofile',
  templateUrl: './updateprofile.component.html',
  styleUrls: ['./updateprofile.component.css']
})
export class UpdateprofileComponent implements OnInit {
  data: any;
  profile:Profile
  @Output() sendData: EventEmitter<any> = new EventEmitter();
  constructor(public helper:HelperService, public router:Router) { }
  ngOnInit(): void {
    console.log('entró');
    let user = JSON.parse(localStorage.getItem('user'))  
    this.helper.get('private/user',{id_user:user.id_user}).then(response=>{
      this.data = response.data
      this.sendData.emit(response.data)
    })
  }

  getData(value){
    value.id_user =this.data.id_user;
    let navigationExtras: NavigationExtras = {
      state: {value:value, type:2}
    };
    this.router.navigate(['/choosejob'],navigationExtras);
    
  }

}
