import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper/helper.service';
import { Room } from 'src/app/models/Room';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-createroom',
  templateUrl: './createroom.component.html',
  styleUrls: ['./createroom.component.css']
})
export class CreateroomComponent implements OnInit {
  rooms: Room[];
  formRoom: FormGroup;
  isUpdate: boolean;
  room: any;
  constructor(public helper:HelperService,public route:ActivatedRoute, public router:Router, private formBuilder: FormBuilder, public loading: LoadingService, public alert:AlertService) { }

  ngOnInit(): void {
    if (typeof this.router.getCurrentNavigation().extras.state !='undefined') {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          let dataRoute=this.router.getCurrentNavigation().extras.state.data;
          this.isUpdate = true;
          this.room = dataRoute
          this.formRoom =  this.formBuilder.group({
            name:[this.room.name, Validators.required]
          })
        }
      });
    }else{
      this.formRoom =  this.formBuilder.group({
        name:['', Validators.required]
      })
    }
  }

  addRoom(){
    this.loading.present()
    let property =  JSON.parse(localStorage.getItem('property'));
    this.helper.post('private/room',{id_property: property.id_property,name:this.formRoom.value.name}).then(response=>{
      if(response.status==1){
        this.alert.presentAlert('Aviso','Se crea Correctamente la habitación')
        this.router.navigate(['/manage/rooms'])
      }
      this.loading.dismiss()
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss()
    })
  }

  updateRoom(){
    let property =  JSON.parse(localStorage.getItem('property'));
    this.helper.put('private/room',{name:this.formRoom.value.name, id_property_room: this.room.id_property_room}).then(response=>{
      if(response.status==1){
        this.alert.presentAlert('Aviso','Se ha actualizadi Correctamente la Habitación')
        this.router.navigate(['/manage/rooms'])
      }
      this.loading.dismiss()
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss();
    })
  }

  deteleRoom(){
    let confirm  = this.alert.presentConfirm('Aviso','¿Desea eliminar la Habitación?','No','Si');
    confirm.then(data=>{
      if (data) {
        this.loading.present()
        this.helper.delete('private/room',this.room).then(response=>{
          if(response.status==1){
            this.alert.presentAlert('Aviso','Se ha Eliminado Correctamente la Habitación.')
            this.router.navigate(['/manage/rooms'])
          }
          this.loading.dismiss()
        }).catch(error=>{
          console.error(error);
          this.loading.dismiss()
        })
      }
    }).catch(error=>{
      console.error(error)
    })
  
  }

}
