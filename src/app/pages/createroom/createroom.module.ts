import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateroomRoutingModule } from './createroom-routing.module';
import { CreateroomComponent } from './createroom.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { CardmanageModule } from 'src/app/components/cardmanage/cardmanage.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';


@NgModule({
  declarations: [CreateroomComponent],
  imports: [
    CommonModule,
    CreateroomRoutingModule,
    HeaderbackbuttonModule,
    CardmanageModule,
    HelperModule,
    FormsModule,
    ReactiveFormsModule,
    SharedTranslateModule
  ]
})
export class CreateroomModule { }
