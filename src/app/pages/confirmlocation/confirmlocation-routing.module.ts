import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmlocationComponent } from './confirmlocation.component';

const routes: Routes = [{ path: '', component: ConfirmlocationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfirmlocationRoutingModule { }
