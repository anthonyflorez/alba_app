import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmlocationRoutingModule } from './confirmlocation-routing.module';
import { ConfirmlocationComponent } from './confirmlocation.component';


@NgModule({
  declarations: [ConfirmlocationComponent],
  imports: [
    CommonModule,
    ConfirmlocationRoutingModule
  ]
})
export class ConfirmlocationModule { }
