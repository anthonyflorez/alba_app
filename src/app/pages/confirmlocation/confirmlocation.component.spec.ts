import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmlocationComponent } from './confirmlocation.component';

describe('ConfirmlocationComponent', () => {
  let component: ConfirmlocationComponent;
  let fixture: ComponentFixture<ConfirmlocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmlocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmlocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
