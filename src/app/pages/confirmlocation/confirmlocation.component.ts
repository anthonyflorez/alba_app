import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmlocation',
  templateUrl: './confirmlocation.component.html',
  styleUrls: ['./confirmlocation.component.css']
})
export class ConfirmlocationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  initMap() {
    //     this.createpropertyform.controls['latitude'].setValue(6.252801387407175);
    // this.createpropertyform.controls['longitude'].setValue(-75.558694144022);
    // The location of Uluru
    var uluru = {lat: 6.252801387407175, lng: -75.558694144022};
    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 18, center: uluru});
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({position: uluru, map: map});
  }


}
