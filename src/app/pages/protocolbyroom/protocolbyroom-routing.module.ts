import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProtocolbyroomComponent } from './protocolbyroom.component';

const routes: Routes = [{ path: '', component: ProtocolbyroomComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtocolbyroomRoutingModule { }
