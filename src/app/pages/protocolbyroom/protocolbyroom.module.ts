import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProtocolbyroomRoutingModule } from './protocolbyroom-routing.module';
import { ProtocolbyroomComponent } from './protocolbyroom.component';
import { HeaderbackbuttonModule } from 'src/app/components/headerbackbutton/headerbackbutton.module';
import { CardmanageModule } from 'src/app/components/cardmanage/cardmanage.module';
import { CardcheckModule } from 'src/app/components/cardcheck/cardcheck.module';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';
import { HelperModule } from 'src/app/services/helper/helper.module';


@NgModule({
  declarations: [ProtocolbyroomComponent],
  imports: [
    CommonModule,
    ProtocolbyroomRoutingModule,
    HeaderbackbuttonModule,
    CardmanageModule,
    CardcheckModule,
    SharedTranslateModule,
    HelperModule
  ]
})
export class ProtocolbyroomModule { }
