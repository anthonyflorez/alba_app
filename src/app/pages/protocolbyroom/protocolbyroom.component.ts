import { Component, OnInit } from '@angular/core';
import { Protocol } from 'src/app/models/Protocols';
import { Router, ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/services/helper/helper.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-protocolbyroom',
  templateUrl: './protocolbyroom.component.html',
  styleUrls: ['./protocolbyroom.component.css']
})
export class ProtocolbyroomComponent implements OnInit {

  protocols:Protocol[] =[
    {id:1, name:"Este es una prueba 1", number_rooms:301},
    {id:2, name:"Este es una prueba 2", number_rooms:302},
    {id:3, name:"Este es una prueba 3", number_rooms:303},
  ];
  protocolByRoom: any;
  constructor(public router:Router, public route:ActivatedRoute, public helper:HelperService, public loading:LoadingService) { }

  ngOnInit(): void {
    if (typeof this.router.getCurrentNavigation().extras != 'undefined') {
      this.protocolByRoom = this.router.getCurrentNavigation().extras
      this.getDataProtocols()
    }
  }
  
  getDataProtocols(){
    this.helper.get('private/protocol_all',{id_property:this.protocolByRoom.room.id_property}).then(response=>{
      this.protocols =  response.data
    }).catch(error=>{
      console.error(error);
      this.loading.dismiss()
    })
  }



  getChangeStatus(value){
    console.log(value);
    
  }
  gotoHome(){
    this.router.navigate(['home']);
  }
  
  gotoReportIssue(){
    this.router.navigate(['issue']);
  }

}
