import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtocolbyroomComponent } from './protocolbyroom.component';

describe('ProtocolbyroomComponent', () => {
  let component: ProtocolbyroomComponent;
  let fixture: ComponentFixture<ProtocolbyroomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtocolbyroomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtocolbyroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
