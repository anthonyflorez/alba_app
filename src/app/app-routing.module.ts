import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'createprofile', loadChildren: () => import('./pages/createprofile/createprofile.module').then(m => m.CreateprofileModule) },
  { path: 'choosejob', loadChildren: () => import('./pages/choosejob/choosejob.module').then(m => m.ChoosejobModule) },
  { path: 'home', loadChildren: () => import('./pages/adminpages/home/home.module').then(m => m.HomeModule) },
  { path: 'properties', loadChildren: () => import('./pages/properties/properties.module').then(m => m.PropertiesModule) },
  { path: 'createproperty', loadChildren: () => import('./pages/createproperty/createproperty.module').then(m => m.CreatepropertyModule) },
  { path: 'chooselocation', loadChildren: () => import('./pages/chooselocation/chooselocation.module').then(m => m.ChooselocationModule) },
  { path: 'admin', loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule) },
  { path: 'manage/:type', loadChildren: () => import('./pages/manage/manage.module').then(m => m.ManageModule) },
  { path: 'managestaff', loadChildren: () => import('./pages/managestaff/managestaff.module').then(m => m.ManagestaffModule) },
  { path: 'reportstaff', loadChildren: () => import('./pages/reportstaff/reportstaff.module').then(m => m.ReportstaffModule) },
  { path: 'confirmlocation', loadChildren: () => import('./pages/confirmlocation/confirmlocation.module').then(m => m.ConfirmlocationModule) },
  { path: 'updateprofile', loadChildren: () => import('./pages/updateprofile/updateprofile.module').then(m => m.UpdateprofileModule) },
  { path: 'createroom', loadChildren: () => import('./pages/createroom/createroom.module').then(m => m.CreateroomModule) },
  { path: 'createprotocol', loadChildren: () => import('./pages/createprotocol/createprotocol.module').then(m => m.CreateprotocolModule) },
  { path: 'protocolbyroom', loadChildren: () => import('./pages/protocolbyroom/protocolbyroom.module').then(m => m.ProtocolbyroomModule) },
  { path: 'issue', loadChildren: () => import('./pages/issue/issue.module').then(m => m.IssueModule) },
  { path: 'addstaffmember', loadChildren: () => import('./pages/addstaffmember/addstaffmember.module').then(m => m.AddstaffmemberModule) },
  { path: 'asignroom', loadChildren: () => import('./pages/asignroom/asignroom.module').then(m => m.AsignRoomModule) },
  { path: 'confirmroom', loadChildren: () => import('./pages/confirmroom/confirmroom.module').then(m => m.ConfirmroomModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
