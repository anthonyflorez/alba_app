import { Output, EventEmitter } from '@angular/core';

export class Item{
    id:number;
    img:string;
    name:string;
    number:number;
    @Output() sendItem:EventEmitter<any> = new EventEmitter()
}