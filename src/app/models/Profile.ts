export  class Profile{
    public first_name:string = ''
    public second_name:string = ''
    public last_name:string = ''
    public second_last_name:string = ''
    public code:string = ''
    public id_country:number = 1
    public phone:string = ''
    public password:string = ''
    public rpassword:string = ''
    public id_rol:number
    public image?:any;
    constructor(){}
}
