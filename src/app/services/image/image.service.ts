import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
   public file:File;
  constructor() { }
  setImage(file:File){
    this.file = file   
  }


  getImage(){
    return this.file
  }

}
