import { NgModule } from '@angular/core';
import { ImageService } from './image.service';



@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [ImageService],
})
export class ImageModule { }
