import { NgModule } from '@angular/core';
import { LoadingService } from './loading.service';



@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [LoadingService],
})
export class LoadingModule { }
