import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(public alertController: AlertController) { }
  async presentAlert(hearder:'Aviso',message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header:hearder,
      // subHeader: 'Subtitle',
      message: message,
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  async presentConfirm(header: any,message: any,cancelText: any,okText: any): Promise<any> {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        header: header,
        message: message,
        buttons: [
          {
            text: cancelText,
            role: 'cancel',
            cssClass: 'secondary',
            handler: (cancel) => {
              resolve(false);
            }
          }, {
            text: okText,
            handler: (ok) => {
              resolve(true)
            }
          }
        ]
      });
      alert.present();
    });
  }



}
