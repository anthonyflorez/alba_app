import { NgModule } from '@angular/core';
import { AlertService } from './alert.service';


@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [AlertService],
})
export class AlertModule { }
