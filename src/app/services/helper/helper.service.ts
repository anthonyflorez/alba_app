import { Injectable } from '@angular/core';
import { urlBase } from '../../config/globals';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServerResponse } from 'src/app/Intefaces/Response';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(public http: HttpClient) {

  }
  /**
   * Return Promise of type get
   * @param url
   * @param Object
   * @param UrlBase
   */
  async get(url = '', data = {}) {
    let params;
    if (data != {}) {
      params = this.jsonToQueryString(data);
    } else {
      params = ''
    }
    let httpOptions = {};
    if(localStorage.getItem('token')) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return await this.http.get<ServerResponse>(urlBase + url + params, httpOptions).toPromise();
  }
  async delete(url = '', data = {}) {
    let params;
    if (data != {}) {
      params = this.jsonToQueryString(data);
    } else {
      params = ''
    }
    let httpOptions = {};
    if(localStorage.getItem('token')) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return await this.http.delete<ServerResponse>(urlBase + url + params, httpOptions).toPromise();
  }

  /**
   * Return Promise of type post
   * @param url
   * @param Object
   * @param UrlBase
   */
  async post(url = '', data = {}) {
    let httpOptions = {};
    if(localStorage.getItem('token')) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return await this.http.post<ServerResponse>(urlBase + url, data, httpOptions).toPromise();
  }

  async put(url = '', data = {}) {
    let httpOptions = {};
    if(localStorage.getItem('token')) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return await this.http.put<ServerResponse>(urlBase + url, data, httpOptions).toPromise();
  }

  /**
   * Metodo para cambia json a string
   * @param json 
   */
  jsonToQueryString(json) {
    return '?' +
      Object.keys(json).map(function (key) {
        return encodeURIComponent(key) + '=' +
          encodeURIComponent(json[key]);
      }).join('&');
  }

  uploadImage(url,blobData, name, ext) {
    let httpOptions = {};
    if(localStorage.getItem('token')) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    const formData = new FormData();
    formData.append('file', blobData, `myimage.${ext}`);
    formData.append('name', name);
    return this.http.post<ServerResponse>( urlBase +url, formData,httpOptions).toPromise();
  }
  
 
  uploadImageFile(url,file: File) {
    let httpOptions = {};
    if(localStorage.getItem('token')) {
      httpOptions = {
        headers: new HttpHeaders({
          //'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          //'Content-Type': 'application/json',
        })
      };
    }
    const ext = file.name.split('.').pop();
    const formData = new FormData();
    formData.append('file', file, `myimage.${ext}`);
    formData.append('name', file.name);
    return this.http.post<ServerResponse>( urlBase +url, formData,httpOptions).toPromise();

  }
}
