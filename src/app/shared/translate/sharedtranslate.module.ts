import { NgModule, ModuleWithProviders } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonModule } from '@angular/common';


export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  }
  
  @NgModule({
    declarations: [],
    imports: [
      CommonModule,
      HttpClientModule,
      TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [HttpClient]
        },
        defaultLanguage: 'es',
        isolate:true
    })
    ],
    exports: [TranslateModule],
  })
  export class SharedTranslateModule {
  
    static forRoot(): ModuleWithProviders {
      return {
        ngModule: SharedTranslateModule,
      }
    }
  }
