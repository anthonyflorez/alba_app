import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-cardcheck',
  templateUrl: './cardcheck.component.html',
  styleUrls: ['./cardcheck.component.css']
})
export class CardcheckComponent implements OnInit {
  @Input('data') data;
  @Input('title') title;
  @Output() getDataEmit :EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
    console.log(this.data);
  }

  changeStatus(value){
    this.getDataEmit.emit(value)
  }
}
