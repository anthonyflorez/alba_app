import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardcheckComponent } from './cardcheck.component';


@NgModule({
  declarations: [CardcheckComponent],
  imports: [
    CommonModule,
  ], exports:[CardcheckComponent]
})
export class CardcheckModule { }
