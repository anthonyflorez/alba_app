import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CardhoursComponent } from './cardcomplete.component';




describe('CardhoursComponent', () => {
  let component: CardhoursComponent;
  let fixture: ComponentFixture<CardhoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardhoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardhoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
