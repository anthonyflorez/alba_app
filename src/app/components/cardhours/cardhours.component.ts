import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cardhours',
  templateUrl: './cardhours.component.html',
  styleUrls: ['./cardhours.component.css']
})
export class CardhoursComponent implements OnInit {
  @Output() getData:EventEmitter<any> = new EventEmitter()
  @Input('data') data;
  constructor() { }

  ngOnInit(): void {
  }
  acceptItem(){
    this.getData.emit(this.data)
  }


}
