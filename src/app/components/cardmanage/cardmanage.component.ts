import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-cardmanage',
  templateUrl: './cardmanage.component.html',
  styleUrls: ['./cardmanage.component.css']
})
export class CardmanageComponent implements OnInit {
  @Input('data') data;
  @Output() getDataEmit :EventEmitter<any> = new EventEmitter();
  @Input('type') type;
  title: any;
  badge: any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.data);
    
    switch (this.type) {
      case "1":
        this.title = this.data.name
        this.badge = this.data.num_protocols
        break;
      case "2":
        this.title = this.data.name
        this.badge = this.data.number_rooms
        break;
      default:
        break;
    }
  }

  getData(){
    this.getDataEmit.emit(this.data)
  }

}
