import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardmanageComponent } from './cardmanage.component';


@NgModule({
  declarations: [CardmanageComponent],
  imports: [
    CommonModule,
  ],
  exports:[CardmanageComponent]
})
export class CardmanageModule { }
