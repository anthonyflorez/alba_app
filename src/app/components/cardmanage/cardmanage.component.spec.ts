import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardmanageComponent } from './cardmanage.component';

describe('CardmanageComponent', () => {
  let component: CardmanageComponent;
  let fixture: ComponentFixture<CardmanageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardmanageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardmanageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
