import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderbackbuttonComponent } from './headerbackbutton.component';

describe('HeaderbackbuttonComponent', () => {
  let component: HeaderbackbuttonComponent;
  let fixture: ComponentFixture<HeaderbackbuttonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderbackbuttonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderbackbuttonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
