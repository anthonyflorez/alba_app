import { NgModule } from '@angular/core';
import { HeaderbackbuttonComponent } from './headerbackbutton.component';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';



@NgModule({
  declarations: [HeaderbackbuttonComponent],
  entryComponents: [],
  imports: [ SharedTranslateModule ],
  providers: [
    
  ],
  exports:[HeaderbackbuttonComponent],
  bootstrap: []
})
export class HeaderbackbuttonModule {}
