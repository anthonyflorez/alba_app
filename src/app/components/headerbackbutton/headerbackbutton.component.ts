import { Component, OnInit, Input } from '@angular/core';
import {Location} from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-headerbackbutton',
  templateUrl: './headerbackbutton.component.html',
  styleUrls: ['./headerbackbutton.component.css']
})
export class HeaderbackbuttonComponent implements OnInit {

  @Input('path') path;

  constructor(private _location: Location, public router: Router) { }

  ngOnInit(): void {
  }

  backClicked() {
    if(this.path){
      this.router.navigate([this.path]);
    } else {
      this._location.back();
    }
    
  }
}
