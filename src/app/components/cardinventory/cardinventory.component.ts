import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cardinventory',
  templateUrl: './cardinventory.component.html',
  styleUrls: ['./cardinventory.component.css']
})
export class CardinventoryComponent implements OnInit {
  @Output() getData:EventEmitter<any> = new EventEmitter()
  @Input('data') data;
  constructor() { }

  ngOnInit(): void {
  }
  acceptItem(){
    this.getData.emit(this.data)
  }


}
