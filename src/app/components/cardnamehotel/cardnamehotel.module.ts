import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardnamehotelComponent } from './cardnamehotel.component';


@NgModule({
  declarations: [CardnamehotelComponent],
  imports: [
    CommonModule,
  ],
  exports:[CardnamehotelComponent]
})
export class CardnamehotelModule { }
