import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cardnamehotel',
  templateUrl: './cardnamehotel.component.html',
  styleUrls: ['./cardnamehotel.component.css']
})
export class CardnamehotelComponent implements OnInit {
  @Input("data") data;
  constructor() {
 
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    setTimeout(() => {
      console.log(this.data);
      
    }, 2000);

  }

}
