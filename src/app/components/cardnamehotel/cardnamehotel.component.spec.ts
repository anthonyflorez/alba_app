import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardnamehotelComponent } from './cardnamehotel.component';

describe('CardnamehotelComponent', () => {
  let component: CardnamehotelComponent;
  let fixture: ComponentFixture<CardnamehotelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardnamehotelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardnamehotelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
