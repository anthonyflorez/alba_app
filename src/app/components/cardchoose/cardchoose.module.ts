import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardchooseComponent } from './cardchoose.component';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';


@NgModule({
  declarations: [CardchooseComponent],
  exports:[CardchooseComponent],
  imports: [
    CommonModule,
    SharedTranslateModule
  ],
  
})
export class CardchooseModule { }
