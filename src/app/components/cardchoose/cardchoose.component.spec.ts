import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardchooseComponent } from './cardchoose.component';

describe('CardchooseComponent', () => {
  let component: CardchooseComponent;
  let fixture: ComponentFixture<CardchooseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardchooseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardchooseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
