import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './tabs.component';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';


@NgModule({
  declarations: [TabsComponent],
  imports: [
    CommonModule,
    SharedTranslateModule
  ],
  exports:[TabsComponent]
})
export class TabsModule { }
