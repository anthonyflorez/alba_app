import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Tab } from 'src/app/models/Tabs';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {
  @Output() Change : EventEmitter<any> = new EventEmitter();
  @Input('data') data;
  tabs:Tab[] = []
  constructor() { }

  ngOnInit(): void {
    this.tabs = this.data 
  }

  clickTab(tab:Tab,i:number){
    this.tabs.forEach((e1,i1)=>{
      this.tabs[i1].selected = false;
    })
    this.tabs[i].selected = true
    this.Change.emit(tab)
  }
}
