import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderwelcomeComponent } from './headerwelcome.component';

describe('HeaderwelcomeComponent', () => {
  let component: HeaderwelcomeComponent;
  let fixture: ComponentFixture<HeaderwelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderwelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderwelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
