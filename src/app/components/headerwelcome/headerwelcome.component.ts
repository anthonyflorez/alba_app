import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-headerwelcome',
  templateUrl: './headerwelcome.component.html',
  styleUrls: ['./headerwelcome.component.css']
})
export class HeaderwelcomeComponent implements OnInit {
  imgSrc: string;
  hasImg: boolean;
  user: any;

  constructor(public router:Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));
    if (typeof this.user.img != 'undefined') {
      this.hasImg = true
    }
    
  }

  gotoProfile(){
    this.router.navigate(['/updateprofile'])
  }

}
