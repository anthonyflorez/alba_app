import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderwelcomeComponent } from './headerwelcome.component';


@NgModule({
  declarations: [HeaderwelcomeComponent],
  imports: [
    CommonModule,
  ],
  exports:[HeaderwelcomeComponent]
})
export class HeaderwelcomeModule { }
