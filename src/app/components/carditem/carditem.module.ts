import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarditemComponent } from './carditem.component';


@NgModule({
  declarations: [CarditemComponent],
  imports: [
    CommonModule,
  ], 
  exports:[CarditemComponent],
})
export class CarditemModule { }
