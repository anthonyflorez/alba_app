import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormuserComponent } from './formuser.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedTranslateModule } from 'src/app/shared/translate/sharedtranslate.module';
import { HelperModule } from 'src/app/services/helper/helper.module';
import { ImageModule } from 'src/app/services/image/image.module';
import { DataModule } from 'src/app/services/data/data.module';
import { DataService } from 'src/app/services/data/data.service';


@NgModule({
  declarations: [FormuserComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedTranslateModule,
    HelperModule,
    ImageModule,
  ], exports:[FormuserComponent],
  providers:[DataService]
})
export class FormuserModule { }
