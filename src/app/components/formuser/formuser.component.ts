import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Profile } from 'src/app/models/Profile';
import { Router } from '@angular/router';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { ActionSheetController, Platform } from '@ionic/angular';
import { HelperService } from 'src/app/services/helper/helper.service';
import { ImageService } from 'src/app/services/image/image.service';
import { DataService } from 'src/app/services/data/data.service';
const { Camera } = Plugins;

@Component({
  selector: 'app-formuser',
  templateUrl: './formuser.component.html',
  styleUrls: ['./formuser.component.css']
})
export class FormuserComponent implements OnInit {
  @Input("data") data;
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  countries: { name: string; flag: string; }[];
  registeredForm: FormGroup;
  profile: Profile = new Profile;
  @Output() sendDataUser: EventEmitter<any> = new EventEmitter();
  ischarge: boolean;
  srcImg: SafeResourceUrl = 'assets/img/user.svg'
  objectImage: any;
  constructor(public router: Router, public datas:DataService, public formBuilder: FormBuilder,public imageservice:ImageService, public helper:HelperService, private plt: Platform, private actionSheetCtrl: ActionSheetController) {



  }


  ngOnInit(): void {



  }

  ngAfterViewInit() {
    console.log(this.data);
    
    switch (this.data) {
      case 1:
        this.registeredForm = this.formBuilder.group({
          name: ['', Validators.required],
          last_name: ['', Validators.required],
          password: ['', Validators.required],
          code: ['', Validators.required],
          phone: ['', Validators.required],
          id_country: [1, Validators.required],
        });
        this.ischarge = true;
      break;
      case 2:
        let user = JSON.parse(localStorage.getItem('user'))  
        this.helper.get('private/user',{id_user:user.id_user}).then(response=>{
          let user = response.data
          this.registeredForm = this.formBuilder.group({
            name: [user.name, Validators.required],
            last_name: [user.last_name, Validators.required],
            code: [user.code, Validators.required],
            phone: [user.phone, Validators.required],
            id_country: [1, Validators.required],
          });
          this.ischarge = true;
        })
        break;
    
      default:
        
        break;
    }
  }

  sendData() {
    if (this.registeredForm.valid) {
      this.sendDataUser.emit({form:this.registeredForm.value, image:this.objectImage});
    }
  }



  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Base64,
    });
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.webPath;
    // Can be set to the src of an image now
    this.srcImg = imageUrl;
  }
  async selectImageSource() {
    const buttons = [
      {
        text: 'Take Photo',
        icon: 'camera',
        handler: () => {
          this.addImage(CameraSource.Camera);
        }
      },
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.addImage(CameraSource.Photos);
        }
      }
    ];

    // Only allow file selection inside a browser
    if (!this.plt.is('hybrid')) {
      buttons.push({
        text: 'Choose a File',
        icon: 'attach',
        handler: () => {
          this.fileInput.nativeElement.click();
        }
      });
    }

    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }


  async addImage(source: CameraSource) {
    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: true,
      resultType: CameraResultType.Base64,
      source
    });
    var imageUrl = 'data:image/png;base64,'+image.base64String;
    this.srcImg = imageUrl;
    const blobData = this.b64toBlob(image.base64String, `image/${image.format}`);
    const imageName = 'Give me a name';
    this.objectImage = { type:1,blobData:blobData, imageName:imageName, format:image.format }
    
    // this.helper.uploadImage('prueba',blobData, imageName, image.format).then(response=>{
    //   console.log(response);
      
    // })


  }

  // Used for browser direct file upload
  uploadFile(event: EventTarget) {
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;    
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const file: File = target.files[0];
    var reader = new FileReader();

    var imgtag = document.getElementById("myimage") as HTMLInputElement;
    imgtag.title = file.name;
    var _this = this
    reader.onload = function(event) {
      imgtag.src  = event.target.result as any;
      // _this.srcImg = event.target.result;
    };   
    
    reader.readAsDataURL(file);
    this.imageservice.setImage(file)
    this.datas.setData(file)
    this.objectImage = {file, type:2}
    this.registeredForm.value.image = {file, type:2}
  }


  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
}
