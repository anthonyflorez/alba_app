import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardcompleteComponent } from './cardcomplete.component';




@NgModule({
  declarations: [CardcompleteComponent],
  imports: [
    CommonModule,
  ],
  exports:[CardcompleteComponent],
})
export class CardcompleteModule { }
