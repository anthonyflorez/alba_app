import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CardcompleteComponent } from './cardcomplete.component';



describe('CardcompleteComponent', () => {
  let component: CardcompleteComponent;
  let fixture: ComponentFixture<CardcompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardcompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardcompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
