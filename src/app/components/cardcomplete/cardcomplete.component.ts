import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cardcomplete',
  templateUrl: './cardcomplete.component.html',
  styleUrls: ['./cardcomplete.component.css']
})
export class CardcompleteComponent implements OnInit {
  @Output() getData:EventEmitter<any> = new EventEmitter()
  @Input('data') data;
  @Input('type') type;
  constructor() { }

  ngOnInit(): void {
  }
  acceptItem(){
    this.getData.emit(this.data)
  }


}
