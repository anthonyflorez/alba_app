import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment'; 

@Component({
  selector: 'app-carditemtodo',
  templateUrl: './carditemtodo.component.html',
  styleUrls: ['./carditemtodo.component.css']
})
export class CarditemtodoComponent implements OnInit {
  @Output() getData:EventEmitter<any> = new EventEmitter()
  @Input('data') data;
  @Input('type') type;
  isDefined: boolean;
  constructor() { }

  ngOnInit(): void {
    //Esto es la diferencia de fecha
    var date1 = moment(this.data.end_time);
    var date2 = moment(this.data.start_time);
    var diff =  date1.diff(date2)
    var duration = moment.duration(diff);
    var hours = duration.asHours();
    var minutes = duration.asMinutes() %60;
    var seconds = duration.asSeconds() %60;
    
    console.log(Math.trunc(hours) + ' hour and '+Math.trunc(minutes) + 'minutes.' + seconds + 'seconds');
    
    
    if (typeof this.data != 'undefined') {
      this.isDefined = true
    }else{
      this.isDefined = false
    }
  }
  acceptItem(){
    this.getData.emit(this.data)
  }


}
