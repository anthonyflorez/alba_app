import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarditemtodoComponent } from './carditemtodo.component';


@NgModule({
  declarations: [CarditemtodoComponent],
  imports: [
    CommonModule,
  ],
  exports:[CarditemtodoComponent],
})
export class CarditemtodoModule { }
